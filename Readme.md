# Scala Programming Exercise - Cars

## Task: Create classes and tests to represent the following:

* A car is used to transport people from one location to another
* Each car has a maximum capacity which cannot be exceeded
* In order to go from one place to another a car needs a driver
* A driver must be 17 and have a driving licence

## The Problem: Satisfy the following

* A new car starts at the location 'Home'
* A car has a _capacity_. This can be different for different cars (this is done as a constructor parameter)
    * See page 139 of Atomic Scala - **Class Arguments**
* A new car has no passengers, i.e. it is empty
* A car can `driveTo` a new location only if it has a driver
  * What happens if there is no driver (how should the code behave)
* A car will end up at its new location once it has been driven there
* A car can have passengers added to it until its capacity is full.
* A passenger cannot be a driver
* Passengers under 10 cannot sit in the front seats

### Starters
* Create classes for Car and Person
  These have already been started for you in `src/main/scala/exercises/cars` but will need to be changed
    * You _may_ need to create new classes. They can be created in the
* Write tests for your classes in `src/test/scala/exercises/cars`. Again some empty ones have been
  added but will need to be filled in.
* Each will have fields to hold some state
* Classes can be created with fields already defined by using a _constructor_ with parameters
  E.g.
  ```
class Person(name: String, age: Int) {

}

val child = new Person("Little Billy", 6)
```
* You can use a `Vector` to contain the list of passengers.
  However if you do it will need to be a `var` and you will need to add values like this
  ```
  // Create the vector
  var passengers = Vector.empty[Person]

  // updating it by setting the value at an index
  passengers = passengers.updated(index, child)
  // updating it by adding a new value to the fromt
  passengers = child +: passengers
  ```
* You will need to use conditional checks to ensure the capacity.
  The `size` of the vector is the number of passengers it currently contains
* To test if a vector contains certain elements you can use this:
  ```
  val people = Vector("Alice", "Bob", "Charles", "Dotty")

  people should contain allOf("Bob", "Dotty", "Charles", "Alice")
  people should not contain("Eugene")
  people should contain inOrder("Bob", "Dotty")
  ```

### Writing a Test
The test classes all start with `class <Something>Spec extends FlatSpec with Matchers {`. If you
add new classes add a new test in the same way.

To write a test use the following style:

```
"a Car" should "start with its location as Home" in {
  val car = new Car
  car.location should be("Home")
}
```

Write tests that show the functions working.
These are a very small sample of the type of things you _could_ write.

```
val car = new Car...
car.location should be("Home")
car.passengers should be(empty)
car.hasDriver should be(false)
// add people and driver
car.driveTo("Far Away")
car.location should be("Far Away")
```

## Possible Extensions
* People could also have a location which they change when they are driven there
* Add fuel to the `Car` class
    * The car cannot drive unless there is fuel in it - is there a minimum fullness?
    * Driving to a new location uses up fuel
    * What happens if there is not enough fuel to get to the location?
    * How do you refuel the car?
* Are there any other collection types you could use to store the passengers? Are any better or worse?
