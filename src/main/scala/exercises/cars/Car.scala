package exercises.cars

/**
  * Add your definition of a Car here. You can add any methods and fields that you like
  */
class Car (var capacity:Int = 5) {
  var location = "Home"
 // val destlocation = "Far Away"
  var driver = Vector.empty[Person]
  var passengers = Vector.empty[Person]
  val maxChild = 2


  def addPassengers(newPassenger: Person) = {
    if (isFull) println("Car is Full")
    else {
      passengers = newPassenger +: passengers
    }
  }

  def isFull:Boolean = passengers.size == capacity -1
  def isChild(person: Person) = person.age < 10

  def driveTo(destination: String):Unit = {
    if (hasDriver) {
      location = destination
    }
    else println("Unable to drive without Driver")
  }
 def hasDriver: Boolean = driver.nonEmpty

  def addDriver(drivername:Person) = {
    driver = drivername +: driver

  }

 //def hasDriver: Boolean = true

  def hasNoDriver: Boolean = true
}



//def maxSeatingCapacity (capacity: Int):Int


