package exercises.cars

import org.scalatest.{FlatSpec, Matchers}

import scala.Option

/**
  * These are (will be) the tests for a Car and all its behaviour
  */
class CarSpec extends FlatSpec with Matchers {

  "a Car" should "start with its location as Home" in {
    val car = new Car
    car.location should be("Home")
  }

  "a Car" should "have no passengers when created" in {
    val car = new Car
    car.passengers should be(empty)
  }



  "a Car" should "have no driver when created" in {
    val car = new Car
    car.hasDriver should be(false)
  }


  "a Car" should "remain at home if no driver" in {
    val car = new Car
    car.location should be("Home")
  }


  "Drive Car" should "take you to destination" in {
    val car = new Car (5)
    car.addDriver(new Person("chandra" , 12))

    if (car.hasDriver)
      {
        car.driveTo("Far Away")
        car.location should be("Far Away")
      }
    else
    println("Car does not have a driver ")
  }



}
