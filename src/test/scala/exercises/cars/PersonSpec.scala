package exercises.cars

import org.scalatest.{FlatSpec, Matchers}

/**
  * These are (will be) the tests for a Person and all its behaviour
  */
class PersonSpec extends FlatSpec with Matchers {


  "addPassenger" should "add new passengers to car" in {
    val car = new Car(4)
    val Alice =  new Person("Alice",14)
    val Bob =  new Person("Bob",20)
    val Charles =  new Person("Charles",21)
    val Dotty =  new Person("Dotty",22)
    val Eugene = new Person("Eugene",29)
    car.addPassengers(Alice)
    car.addPassengers(Bob)
    car.addPassengers(Charles)
    //car.addPassengers(Dotty)
    //car.addPassengers(Eugene)
//println(car.passengers)
    car.passengers should contain allOf(Alice, Bob, Charles)
    //car.passengers should not contain(Eugene)
  }

}


